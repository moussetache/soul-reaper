import React from "react";
import "./modal.css";

const Modal = ({children, className, open, onClose}) => {
    if(!open) {
        return null;
    }
    
    return (
        <div className={`modal ${className}`}>
            <div class="modal-content">
                <span class="close" onClick={onClose}>&times;</span>
                {children}
            </div>
        </div>
    )
};

export default Modal;