import React, { useContext, useState } from "react";

import GameStateContext from "@soul/contexts/gameStateContext";

import Button from "@soul/components/Button/button";
import Modal from "@soul/components/modal/Modal";

import "./settings.css";

const defaultState = {souls:0, soulsPerClick:1, humans: 2, upgrades: {}};

const Settings = () => {
    const [open, setOpen] = useState(false);
    const {gameState, setGameState} = useContext(GameStateContext);
    const [gameSaved, setGameSaved] = useState(false);

    const WipeSave = async() => {
        await setGameState(defaultState);
        setOpen(false);
    };

    const SaveGame = async() => {
        await localStorage.setItem("soulReaperSave", JSON.stringify(gameState));
        setGameSaved(true);
    };

    return (
        <div>
            <h2>
                Settings
            </h2>
            <div className="settings-list">
                <div className="settings-list-item">
                    <Button onClick={() => SaveGame()}>
                        Save Game
                    </Button>
                    {gameSaved && "Game has been saved"}
                </div>
                <Button className="settings-list-item" onClick={() => setOpen(true)}>
                    Reset Save
                </Button>
                <Modal open={open} onClose={() => setOpen(false)}>
                    <div className="confirm-wipe-modal">
                        <div className="confirm-wipe-items">
                            Are you damn sure you want to wipe that save?
                        </div>
                        <div className="confirm-wipe-items">
                            <Button className="confirm-wipe-button" onClick={() => WipeSave()}>
                                Hell yeah
                            </Button>
                        </div>
                        <div className="confirm-wipe-items">
                            <Button onClick={() => setOpen(false)}>
                                Hell no
                            </Button>
                        </div>
                    </div>
                </Modal>
            </div>
        </div>
    )

};

export default Settings;