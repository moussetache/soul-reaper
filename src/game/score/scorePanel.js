import React, { useContext, useEffect, useState } from 'react';
import GameStateContext from '@soul/contexts/gameStateContext';

import Number from '@soul/components/Number/number';

import "./score.css"

const ScorePanel = ({className}) => {
    const { gameState, setGameState } = useContext(GameStateContext);
    const { souls, humans, basePopModifier, maxPop } = gameState;
    const [reproduce, setReproduce] = useState(false);

    
    useEffect(() => {
        setInterval(() => {
            setReproduce(true);
        }, 1000);
      }, []);

    useEffect(() =>  {
        if(reproduce) {
            var newBasePopModifier = basePopModifier ? basePopModifier : 1;
            var births = Math.ceil((humans*Math.pow(1.002,newBasePopModifier)));
            var maxPop = 10*Math.pow(2,newBasePopModifier);
            var newHumans = (gameState.soulsPerSecond > 0)  ? 
                                (humans > gameState.soulsPerSecond?
                                    Math.ceil(births-gameState.soulsPerSecond):
                                    0) :
                                Math.ceil(births);
            newHumans = newHumans > maxPop ? maxPop : newHumans;       

            var newSouls =  (gameState.soulsPerSecond) ? 
                                (humans > gameState.soulsPerSecond ? 
                                    gameState.souls+gameState.soulsPerSecond : 
                                    gameState.souls+humans) : 
                                gameState.souls;

            var humansPerSecond = newHumans == maxPop ? gameState.soulsPerSecond : births-humans;

            setGameState({...gameState, 
                basePopModifier: newBasePopModifier,
                humans: newHumans,
                humansPerSecond,
                maxPop,
                deaths: newSouls - souls,
                souls: newSouls});
            setReproduce(false);
        }
    }, [reproduce]);
    return (
        <div className={`${className} score-panel-container`}>
            <div className="score-scouls">
                <b>Souls</b>
                <Number number={souls} />
            </div>
            <div className="score-scouls">
                <b>Humans</b>
                <div>
                    <Number number={humans} /> / <Number number={maxPop} />
                </div>
            </div>
        </div>
    );
};

export default ScorePanel;