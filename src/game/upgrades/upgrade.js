import Button from '@soul/components/Button/button';
import Number from '@soul/components/Number/number';
import Tooltip from '@soul/components/Tooltip/tooltip';
import React from 'react';

const Upgrade = ({name, disabled, cost, currency, unlockFunction, tooltip}) => {
    return (
        <div className="upgrades">                
            <Tooltip text={tooltip}>
                <Button onClick={() => unlockFunction()} disabled={disabled}>
                    {name}
                </Button>
            </Tooltip>
            
            <span>Cost: <b><Number number={cost} /></b>{` ${currency ? currency : ""}`}</span>
        </div>
    );
};

export default Upgrade;