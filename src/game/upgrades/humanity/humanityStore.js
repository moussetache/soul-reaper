import React, { useContext, useEffect, useState } from 'react';
import GameStateContext from '@soul/contexts/gameStateContext';
import Upgrade from '../upgrade';
import Upgrades from './humanityUpgrades';

import "./humanity.css";

const HumanityStore = ({className}) => {    
    const { gameState, setGameState } = useContext(GameStateContext);
    const { basePopModifier, humans, upgrades } = gameState;
    const [age, setAge] = useState("the Neolithic");

    const unlock = async(name, cost, humanModifier) => {
        if (humans >= cost && humans > 0) {
            var newState = {...gameState, 
                humans: humans-cost,
                basePopModifier: basePopModifier+humanModifier,
            };
            
            newState.upgrades[name.toLocaleLowerCase()] = true;
            await setGameState(newState);
        }
    };

    useEffect(() => {        
        setAge("the Neolithic");
    }, [gameState.upgrades]);
    
    return (
        <div className={`${className} ${age.toLocaleLowerCase()}`}>
                <b>Humanity is in {age}</b>
                {
                    Upgrades.map((x, i) => {
                        if((i !== 0 && !upgrades[Upgrades[i-1].id]) || upgrades[x.id]) {
                            return null;
                        }

                        return <Upgrade 
                            key={x.id}
                            name={x.name} 
                            tooltip={x.tooltip}
                            cost={x.cost} 
                            unlockFunction={() => unlock(x.id, x.cost, x.bonus)} 
                            currency="humans"
                            disabled={humans < x.cost} />
                    })
                }
        </div>
    );
};

export default HumanityStore;