const Upgrades = [
    {
        id: "foraging",
        name: "Foraging",
        tooltip: "Humanity's reproduction capabilities will be able to grow twice further",
        cost: 10,
        bonus: 1
    }, 
    {
        id: "tools",
        name: "Foraging",
        tooltip: "Humanity's reproduction capabilities will be able to grow twice further",
        cost: 15,
        bonus: 1
    }, 
    {
        id: "huts",
        name: "Huts",
        tooltip: "Humanity's reproduction capabilities will be able to grow twice further",
        cost: 50,
        bonus: 1
    }, 
    {
        id: "fire",
        name: "Fire",
        tooltip: "Humanity's reproduction capabilities will be able to grow thrice further",
        cost: 90,
        bonus: 2
    }, 
    {
        id: "twins",
        name: "Twins",
        tooltip: "Humanity's reproduction capabilities will be able to grow twice further",
        cost: 150,
        bonus: 1
    }, 
    {
        id: "fishing",
        name: "Fishing",
        tooltip: "Humanity's reproduction capabilities will be able to grow thrice further",
        cost: 200,
        bonus: 2
    },
    {
        id: "sailing",
        name: "Sailing",
        tooltip: "Humanity's reproduction capabilities will be able to grow twice further",
        cost: 400,
        bonus: 1
    }, 
    {
        id: "agriculture",
        name: "Agriculture",
        tooltip: "Humanity's reproduction capabilities will be able to grow twice further",
        cost: 800,
        bonus: 1
    }, 
    {
        id: "domestication",
        name: "Friendly sheeps",
        tooltip: "Thanks to wool, warmth and frienship, humanity's reproduction capabilities will be able to grow thrice further",
        cost: 1200,
        bonus: 2
    }, 
    {
        id: "rice",
        name: "Friendly rice",
        tooltip: "Uncle Jimmy claims plants are his friends too ¯\\_(ツ)_/¯. Humanity's reproduction capabilities will be able to grow twice further",
        cost: 2000,
        bonus: 1
    }, 
    {
        id: "settlements",
        name: "Settlements",
        tooltip: "We're now stuck with uncle Jimmy. Humanity's reproduction capabilities will be able to grow twice further",
        cost: 2800,
        bonus: 1
    }, 
    {
        id: "fermentation",
        name: "Fermentation",
        tooltip: "Rice smelled weird, but Jimmy insisted it tasted nice. Humanity's reproduction capabilities will be able to grow twice further",
        cost: 4000,
        bonus: 1
    }, 
    {
        id: "leather",
        name: "Leather",
        tooltip: "If the wine rice smelled bad, that was worse, but eh it's warmer than without. Plus, Jimmy says we look dope. Humanity's reproduction capabilities will be able to grow twice further",
        cost: 8000,
        bonus: 1
    }, 
    {
        id: "mudbricks",
        name: "Mud bricks",
        tooltip: "Humanity's reproduction capabilities will be able to grow twice further",
        cost: 14000,
        bonus: 1
    },
    {
        id: "dogs",
        name: "Dogs",
        tooltip: "WILL YOU LOOK AT THOSE PAWS. Humanity's reproduction capabilities will be able to grow half further",
        cost: 23000,
        bonus: .5
    },
    {
        id: "smelting",
        name: "Smelting",
        tooltip: "So much fire in such a small space. Humanity's reproduction capabilities will be able to grow half further",
        cost: 30000,
        bonus: .5
    },
    {
        id: "irrigation",
        name: "Irrigation",
        tooltip: "You know how much time it took me? The river was SO far. Humanity's reproduction capabilities will be able to grow half further",
        cost: 50000,
        bonus: .5
    },
    {
        id: "cotton",
        name: "Cotton threading",
        tooltip: "If you think really hard about it, I can make you shirt out of that bug-ridden plant. Humanity's reproduction capabilities will be able to grow twice further",
        cost: 70000,
        bonus: 1
    },
    {
        id: "oars",
        name: "Oars",
        tooltip: "Jimmy got that wicked idea. What if we used something else than our hands to paddle? Humanity's reproduction capabilities will be able to grow twice further",
        cost: 110000,
        bonus: 1
    },
    {
        id: "candles",
        name: "Candles",
        tooltip: "I don't want to know where you got so much wax. Humanity's reproduction capabilities will be able to grow twice further",
        cost: 160000,
        bonus: 1
    },
    {
        id: "roads",
        name: "Paved roads",
        tooltip: "The road to hell is paved with good intentions. Humanity's reproduction capabilities will be able to grow twice further",
        cost: 200000,
        bonus: 1
    },
    {
        id: "plumbing",
        name: "Plumbing",
        tooltip: "What do you mean the water comes to me? Humanity's reproduction capabilities will be able to grow twice further",
        cost: 300000,
        bonus: 1
    },
    {
        id: "wheel",
        name: "The Wheel",
        tooltip: "Why is the cart never mentioned? Am I supposed to picture Jimmy on a uniwheel going down the road? Humanity's reproduction capabilities will be able to grow twice further",
        cost: 400000,
        bonus: 1
    },
    {
        id: "silk",
        name: "Silk garments",
        tooltip: "*sigh* Jimmy found some bugs and wants to use their poop to dress ourselves. Humanity's reproduction capabilities will be able to grow half further",
        cost: 600000,
        bonus: .5
    },
    {
        id: "horses",
        name: "Domestication of horses",
        tooltip: "Humanity's reproduction capabilities will be able to grow half further",
        cost: 900000,
        bonus: .5
    },
];
   
export default Upgrades;