const Circles = [
    "Limbo",
    "Lust",
    "Gluttony",
    "Avarice and prodigality",
    "Wrath and sullenness",
    "Heresy",
    "Violence",
    "Fraud",
    "Treachery"
];

export default Circles;