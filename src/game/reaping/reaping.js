import React, { useContext } from 'react';

import GameStateContext from "@soul/contexts/gameStateContext";

import Button from '@soul/components/Button/button';

import Faux from "@soul/assets/faux.png";

import "./reaping.css"

const Reaping = ({}) => {
    const { gameState, setGameState } = useContext(GameStateContext);
    
    const increment = () => {
        if(soulsPerClick < humans -2 ) {
            setGameState({...gameState, souls: gameState.souls+soulsPerClick, humans: humans - soulsPerClick});
        }
        else {
            setGameState({...gameState, souls: gameState.souls+humans, humans: 0});
        }
    };

    const { soulsPerClick, humans, deaths, humansPerSecond } = gameState;

    return (
        <div className="reaping-container">
            <div className="reaping-item">
                <div>
                    <b>{(deaths > 10000) ? (deaths.toExponential(2) ?? 0) : (deaths ?? 0)} soul(s)</b> reaped per second
                </div>
                <div>
                    <b>{(humansPerSecond > 10000) ? (humansPerSecond.toExponential(2) ?? 0) : (humansPerSecond ?? 0)} human(s)</b> born each second
                </div>
            </div>
            <Button className="reaping-item reaping-button"
                onClick={() => increment()}>
                <img src={Faux} alt="faux" />
            </Button>
        </div>               
    );
};

export default Reaping;