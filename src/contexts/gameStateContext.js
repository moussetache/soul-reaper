import React from "react";

const GameStateContext = React.createContext({souls:0, soulsPerClick:1});

export default GameStateContext;