module.exports = {
  pathPrefix: `/soul-reaper`,
  siteMetadata: {
    title: `Soul Reaper`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
