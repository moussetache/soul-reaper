const path = require("path");
exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    resolve: {
      alias: {
        "@soul": path.resolve(__dirname, "src")
      }
    }
  });
};